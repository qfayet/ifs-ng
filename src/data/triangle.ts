export const TRIANGLE = {
  "name": "Triangle",
  "functions": [
    {
      "color": "#00FF00",
      "coefs": [
        [
          0.5,
          0.0,
          0.0
        ],
        [
          0.0,
          0.5,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#0000FF",
      "coefs": [
        [
          0.5,
          0.0,
          -0.5
        ],
        [
          0.0,
          0.5,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.5,
          0.0,
          0.0
        ],
        [
          0.0,
          0.5,
          -0.5
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -1.0,
    "xMax": 0.0,
    "yMin": -1.0,
    "yMax": 0.0
  },
  "background": "#000000",
  "intensity": 2.5,
  "contrast": 1.0,
  "fill_frame": true
}