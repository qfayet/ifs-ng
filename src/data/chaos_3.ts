export const CHAOS_3 = {
  "name": "Chaos 3",
  "functions": [
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.309,
          -0.225,
          0.0
        ],
        [
          0.225,
          0.309,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FFFF00",
      "coefs": [
        [
          -0.118,
          -0.363,
          0.309
        ],
        [
          0.363,
          -0.118,
          0.225
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FF00",
      "coefs": [
        [
          0.309,
          0.225,
          0.191
        ],
        [
          -0.225,
          0.309,
          0.588
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FFFF",
      "coefs": [
        [
          -0.118,
          0.363,
          0.5
        ],
        [
          -0.363,
          -0.118,
          0.363
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#0000FF",
      "coefs": [
        [
          0.309,
          0.225,
          0.382
        ],
        [
          -0.225,
          0.309,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF00FF",
      "coefs": [
        [
          0.309,
          -0.225,
          0.691
        ],
        [
          0.225,
          0.309,
          -0.225
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -0.2,
    "xMax": 1.1,
    "yMin": -0.3,
    "yMax": 0.8
  },
  "background": "#000000",
  "intensity": 2.5,
  "contrast": 1.0,
  "fill_frame": false
}