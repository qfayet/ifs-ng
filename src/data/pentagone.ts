export const PENTAGONE = {
  "name": "Pentagone",
  "functions": [
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.382,
          0.0,
          0.0
        ],
        [
          0.0,
          0.382,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FFFF00",
      "coefs": [
        [
          0.382,
          0.0,
          0.618
        ],
        [
          0.0,
          0.382,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FF00",
      "coefs": [
        [
          0.382,
          0.0,
          0.809
        ],
        [
          0.0,
          0.382,
          0.588
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FFFF",
      "coefs": [
        [
          0.382,
          0.0,
          0.309
        ],
        [
          0.0,
          0.382,
          0.951
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#0000FF",
      "coefs": [
        [
          0.382,
          0.0,
          -0.191
        ],
        [
          0.0,
          0.382,
          0.588
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -0.35,
    "xMax": 1.35,
    "yMin": 0.0,
    "yMax": 1.55
  },
  "background": "#000000",
  "intensity": 2.5,
  "contrast": 1.0,
  "fill_frame": false
}