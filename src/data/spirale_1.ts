export const SPIRALE_1 = {
  "name": "Spirale 1",
  "functions": [
    {
      "color": "#FFFF00",
      "#comment": {},
      "coefs": [
        [
          -0.296,
          0.0469,
          0.5687
        ],
        [
          -0.0469,
          -0.296,
          -0.3791
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#F9FF00",
      "coefs": [
        [
          0.8302,
          0.4091,
          0.3319
        ],
        [
          -0.4091,
          0.8302,
          0.0674
        ]
      ],
      "proba": 7.0
    }
  ],
  "bounds": {
    "xMin": 0.15,
    "xMax": 0.85,
    "yMin": -0.95,
    "yMax": -0.1
  },
  "background": "#000000",
  "intensity": 2.5,
  "contrast": 1.0,
  "fill_frame": false
}