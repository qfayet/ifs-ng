export const DRAGON = {
  "name": "Dragon",
  "functions": [
    {
      "color": "#FFFFFF",
      "coefs": [
        [
          0.5,
          -0.5,
          0.5
        ],
        [
          0.5,
          0.5,
          0.5
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          -0.5,
          -0.5,
          0.5
        ],
        [
          0.5,
          -0.5,
          0.5
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -0.5,
    "xMax": 0.8,
    "yMin": -0.3,
    "yMax": 1.5
  },
  "background": "#000000",
  "intensity": 2.0,
  "contrast": 1.0,
  "fill_frame": false
}