export const FOUGERE = {
  "name": "Fougère",
  "functions": [
    {
      "color": "#AAAA00",
      "coefs": [
        [
          0.0,
          0.0,
          -0.0
        ],
        [
          0.0,
          0.16,
          -0.0
        ]
      ],
      "proba": 0.1
    },
    {
      "color": "#55AB00",
      "coefs": [
        [
          0.8492,
          -0.0371,
          -0.0
        ],
        [
          0.0371,
          0.8492,
          -1.6
        ]
      ],
      "proba": 0.7
    },
    {
      "color": "#80AA00",
      "coefs": [
        [
          0.1968,
          0.2264,
          -0.0
        ],
        [
          -0.2264,
          0.1968,
          -1.6
        ]
      ],
      "proba": 0.1
    },
    {
      "color": "#80AA00",
      "coefs": [
        [
          -0.15,
          -0.2834,
          -0.0
        ],
        [
          -0.2598,
          0.2378,
          -0.44
        ]
      ],
      "proba": 0.1
    }
  ],
  "bounds": {
    "xMin": -2.7,
    "xMax": 3.3,
    "yMin": -11.0,
    "yMax": 1.0
  },
  "background": "#FFFF99",
  "intensity": 1.0,
  "contrast": 0.1,
  "fill_frame": false
}