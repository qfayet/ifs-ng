export const HEXAGONE_PLEIN_2 = {
  "name": "Hexagone plein 2",
  "functions": [
    {
      "color": "#7F7F7F",
      "coefs": [
        [
          0.666666,
          0.0,
          0.0
        ],
        [
          0.0,
          0.666666,
          0.0
        ]
      ],
      "proba": 6.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.333333,
          0.0,
          1.0
        ],
        [
          0.0,
          0.333333,
          0.57735
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FFFF00",
      "coefs": [
        [
          0.333333,
          0.0,
          0.0
        ],
        [
          0.0,
          0.333333,
          1.1547
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FF00",
      "coefs": [
        [
          0.333333,
          0.0,
          -1.0
        ],
        [
          0.0,
          0.333333,
          0.57735
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FFFF",
      "coefs": [
        [
          0.333333,
          0.0,
          -1.0
        ],
        [
          0.0,
          0.333333,
          -0.57735
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#0000FF",
      "coefs": [
        [
          0.333333,
          0.0,
          0.0
        ],
        [
          0.0,
          0.333333,
          -1.1547
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF00FF",
      "coefs": [
        [
          0.333333,
          0.0,
          1.0
        ],
        [
          0.0,
          0.333333,
          -0.57735
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -1.5,
    "xMax": 1.5,
    "yMin": -1.73205,
    "yMax": 1.73205
  },
  "background": "#000000",
  "intensity": 4.0,
  "contrast": 0.5,
  "fill_frame": false
}