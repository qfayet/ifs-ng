export const CHAOS_4 = {
  "name": "Chaos 4",
  "functions": [
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.341,
          -0.071,
          0.0
        ],
        [
          0.071,
          0.341,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FFFF00",
      "coefs": [
        [
          0.038,
          -0.346,
          0.341
        ],
        [
          0.346,
          0.038,
          0.071
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FF00",
      "coefs": [
        [
          0.341,
          -0.071,
          0.379
        ],
        [
          0.071,
          0.341,
          0.418
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FFFF",
      "coefs": [
        [
          -0.234,
          0.258,
          0.72
        ],
        [
          -0.258,
          -0.234,
          0.489
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#0000FF",
      "coefs": [
        [
          0.173,
          0.302,
          0.486
        ],
        [
          -0.302,
          0.173,
          0.231
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF00FF",
      "coefs": [
        [
          0.341,
          -0.071,
          0.659
        ],
        [
          0.071,
          0.341,
          -0.071
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -0.05,
    "xMax": 1.05,
    "yMin": -0.1,
    "yMax": 0.7
  },
  "background": "#000000",
  "intensity": 2.5,
  "contrast": 1.0,
  "fill_frame": false
}