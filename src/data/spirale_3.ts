export const SPIRALE_3 = {
  "name": "Spirale 3",
  "functions": [
    {
      "color": "#FFFF00",
      "coefs": [
        [
          0.7155,
          -0.4589,
          0.3412
        ],
        [
          0.4589,
          0.7155,
          -0.0939
        ]
      ],
      "proba": 8.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.2362,
          -0.1849,
          0.216
        ],
        [
          0.1849,
          0.2362,
          0.0852
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.2819,
          0.1849,
          0.567
        ],
        [
          0.1025,
          -0.3205,
          0.3792
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.108,
          0.2799,
          0.3303
        ],
        [
          -0.2799,
          0.108,
          0.9098
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -0.0,
    "xMax": 1.0,
    "yMin": -0.0,
    "yMax": 1.0
  },
  "background": "#000000",
  "intensity": 3.0,
  "contrast": 0.66,
  "fill_frame": false
}