export const OCTOGONE_CREUX = {
  "name": "Octogone creux",
  "functions": [
    {
      "color": "#FF7F00",
      "coefs": [
        [
          0.2928932,
          0.0,
          0.5
        ],
        [
          0.0,
          0.2928932,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FFFF00",
      "coefs": [
        [
          0.2928932,
          0.0,
          0.3535533
        ],
        [
          0.0,
          0.2928932,
          0.3535533
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#7FFF7F",
      "coefs": [
        [
          0.2928932,
          0.0,
          0.0
        ],
        [
          0.0,
          0.2928932,
          0.5
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#00FFFF",
      "coefs": [
        [
          0.2928932,
          0.0,
          -0.3535533
        ],
        [
          0.0,
          0.2928932,
          0.3535533
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#007FFF",
      "coefs": [
        [
          0.2928932,
          0.0,
          -0.5
        ],
        [
          0.0,
          0.2928932,
          0.0
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#0000FF",
      "coefs": [
        [
          0.2928932,
          0.0,
          -0.3535533
        ],
        [
          0.0,
          0.2928932,
          -0.3535533
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#7F007F",
      "coefs": [
        [
          0.2928932,
          0.0,
          0.0
        ],
        [
          0.0,
          0.2928932,
          -0.5
        ]
      ],
      "proba": 1.0
    },
    {
      "color": "#FF0000",
      "coefs": [
        [
          0.2928932,
          0.0,
          0.3535533
        ],
        [
          0.0,
          0.2928932,
          -0.3535533
        ]
      ],
      "proba": 1.0
    }
  ],
  "bounds": {
    "xMin": -0.7071067,
    "xMax": 0.7071067,
    "yMin": -0.7071067,
    "yMax": 0.7071067
  },
  "background": "#000000",
  "intensity": 2.5,
  "contrast": 1.0,
  "fill_frame": false
}