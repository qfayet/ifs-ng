export const FEUILLE = {
  "name": "Feuille",
  "functions": [
    {
      "color": "#55AA00",
      "coefs": [
        [
          0.6666666,
          0.0,
          0.0
        ],
        [
          0.0,
          0.6666666,
          -6.0
        ]
      ],
      "proba": 0.15
    },
    {
      "color": "#AAAA00",
      "coefs": [
        [
          0.6666666,
          0.0,
          0.0
        ],
        [
          0.0,
          0.6666666,
          3.0
        ]
      ],
      "proba": 0.4
    },
    {
      "color": "#AAAA00",
      "coefs": [
        [
          0.5,
          -0.5,
          -8.0
        ],
        [
          0.5,
          0.5,
          -6.0
        ]
      ],
      "proba": 0.225
    },
    {
      "color": "#AAAA00",
      "coefs": [
        [
          0.5,
          0.5,
          8.0
        ],
        [
          -0.5,
          0.5,
          -6.0
        ]
      ],
      "proba": 0.225
    }
  ],
  "bounds": {
    "xMin": -15.0,
    "xMax": 15.0,
    "yMin": 10.0,
    "yMax": -20.0
  },
  "background": "#FFFF99",
  "intensity": 1.0,
  "contrast": 0.2,
  "fill_frame": false
}