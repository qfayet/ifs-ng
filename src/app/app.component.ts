import { Component, OnInit } from '@angular/core';
import { IfsService } from '@app/ifs.service';
import { Ifs } from './models/ifs-model';

const INITIAL_IFS_INDEX = 1;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ifs-ng';

  ifsList: Ifs[];
  selectedIfs: Ifs;

  constructor(private ifsService: IfsService) {}

  ngOnInit(): void {
    this.ifsList = this.ifsService.getIfsList();
    this.selectedIfs = this.ifsList[INITIAL_IFS_INDEX];
  }

}
