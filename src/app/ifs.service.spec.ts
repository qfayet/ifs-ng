import { TestBed } from '@angular/core/testing';

import { IfsService } from './ifs.service';

describe('IfsService', () => {
  let service: IfsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IfsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
