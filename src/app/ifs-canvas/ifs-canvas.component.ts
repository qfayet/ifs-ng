import { AfterViewInit, Component, ElementRef, Input, NgZone, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { DrawSampler, DrawSamplerOpti } from '@app/models/draw-sampler';
import { Ifs, Point } from '@app/models/ifs-model';
import { IfsRenderer, IfsRendererScale } from '@app/models/ifs-renderer';

const LOG_DELAY = 1000;
const LOG_DRAW_INFO = false;

const DISPLAY_SCALE = 0.80;


@Component({
  selector: 'app-ifs-canvas',
  templateUrl: './ifs-canvas.component.html',
  styleUrls: ['./ifs-canvas.component.scss']
})
export class IfsCanvasComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('canvas')
  canvasRef: ElementRef;

  @Input()
  ifs: Ifs;

  frameWidth: number;
  frameHeight: number;
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  lastLog: number;

  renderer: IfsRenderer;
  sampler: DrawSampler;

  requestId: any = null;

  constructor(public ngZone: NgZone, private host: ElementRef) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    // @ts-ignore
    const observer = new ResizeObserver(entries => {
      this.onSizeChanged(entries[0].contentRect);
    });
    this.canvas = this.canvasRef.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    observer.observe(this.host.nativeElement);
  }

  onSizeChanged(rect: DOMRect): void {
    this.frameWidth = rect.width * DISPLAY_SCALE;
    this.frameHeight = rect.height * DISPLAY_SCALE;
    this.reload();
  }

  computeViewSize(frameWidth: number, frameHeight: number): Point {
    if (frameWidth <= 0.0 || frameHeight <= 0.0) {
      return new Point();
    }
    if (this.ifs.fill_frame) {
      return new Point(frameWidth, frameHeight);
    }
    let boundsWidth = Math.abs(this.ifs._bounds.getWidth());
    let boundsHeight = Math.abs(this.ifs._bounds.getHeight());
    let frameRatio = frameWidth / frameHeight;
    let boundsRation = boundsWidth / boundsHeight;
    if (frameRatio > boundsRation) {
      return new Point(frameHeight * boundsRation, frameHeight)
    }
    return new Point(frameWidth, frameWidth / boundsRation);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.canvas) {
      this.reload();
    }
  }

  reload(): void {
    this.stop();
    this.start();
  }

  start(): void {
    let viewSize = this.computeViewSize(this.frameWidth, this.frameHeight);
    this.canvas.width = Math.floor(viewSize.x);
    this.canvas.height = Math.floor(viewSize.y);
    this.lastLog = 0;
    this.sampler = new DrawSamplerOpti();
    this.renderer = new IfsRendererScale(this.ifs, this.canvas.width, this.canvas.height);
    this.host.nativeElement.style.setProperty('background-color', this.ifs._background.format());
    this.ctx.fillStyle = this.ifs._background.format();
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ngZone.runOutsideAngular(() => this.requestAnimation());
  }

  stop(): void {
    if (this.requestId !== null) {
      cancelAnimationFrame(this.requestId);
      this.requestId = null;
    }
  }

  animate(time: number): void {
    let nbPoints = this.sampler.getSample(time);
    if (time - this.lastLog > LOG_DELAY) {
      LOG_DRAW_INFO && console.log(time, nbPoints);
      this.lastLog = time;
    }
    const start = Date.now();
    for (let index = 0; index < nbPoints; index++) {
      this.renderer.drawIteration(this.ctx);
    }
    this.renderer.drawFinish(this.ctx);
    this.sampler.setDrawDuration(nbPoints, Date.now() - start);
    this.requestAnimation();
  }

  requestAnimation(): void {
    this.requestId = requestAnimationFrame((time) => this.animate(time));
  }

}
