import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IfsCanvasComponent } from './ifs-canvas.component';

describe('IfsCanvasComponent', () => {
  let component: IfsCanvasComponent;
  let fixture: ComponentFixture<IfsCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IfsCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IfsCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
