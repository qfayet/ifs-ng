import { Injectable } from '@angular/core';
import { CARRE } from '@data/carre';
import { CHAOS_1 } from '@data/chaos_1';
import { CHAOS_2 } from '@data/chaos_2';
import { CHAOS_3 } from '@data/chaos_3';
import { CHAOS_4 } from '@data/chaos_4';
import { CROIX } from '@data/croix';
import { DRAGON } from '@data/dragon';
import { FEUILLE } from '@data/feuille';
import { FOUGERE } from '@data/fougere';
import { HEXAGONE_CREUX } from '@data/hexagone_creux';
import { HEXAGONE_PLEIN_1 } from '@data/hexagone_plein_1';
import { HEXAGONE_PLEIN_2 } from '@data/hexagone_plein_2';
import { OCTOGONE_CREUX } from '@data/octogone_creux';
import { OCTOGONE_PLEIN_1 } from '@data/octogone_plein_1';
import { OCTOGONE_PLEIN_2 } from '@data/octogone_plein_2';
import { PENTAGONE } from '@data/pentagone';
import { SPIRALE_1 } from '@data/spirale_1';
import { SPIRALE_2 } from '@data/spirale_2';
import { SPIRALE_3 } from '@data/spirale_3';
import { SPIRALE_4 } from '@data/spirale_4';
import { TRIANGLE } from '@data/triangle';
import { Ifs } from './models/ifs-model';


export const IFS_LIST: Ifs[] = [
  Ifs.parse(CARRE),
  Ifs.parse(CHAOS_1),
  Ifs.parse(CHAOS_2),
  Ifs.parse(CHAOS_3),
  Ifs.parse(CHAOS_4),
  Ifs.parse(CROIX),
  Ifs.parse(DRAGON),
  Ifs.parse(FEUILLE),
  Ifs.parse(FOUGERE),
  Ifs.parse(HEXAGONE_CREUX),
  Ifs.parse(HEXAGONE_PLEIN_1),
  Ifs.parse(HEXAGONE_PLEIN_2),
  Ifs.parse(OCTOGONE_CREUX),
  Ifs.parse(OCTOGONE_PLEIN_1),
  Ifs.parse(OCTOGONE_PLEIN_2),
  Ifs.parse(PENTAGONE),
  Ifs.parse(SPIRALE_1),
  Ifs.parse(SPIRALE_2),
  Ifs.parse(SPIRALE_3),
  Ifs.parse(SPIRALE_4),
  Ifs.parse(TRIANGLE),
];


@Injectable({
  providedIn: 'root'
})
export class IfsService {

  constructor() { }

  getIfsList(): Ifs[] {
    return IFS_LIST;
  }
}
