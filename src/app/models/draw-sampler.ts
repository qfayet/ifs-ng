
const REFRESH_DELAY: number = 1000 / 20;  // 20 images/seconde
const INITIAL_POINTS: number = 1000;
const DEFAULT_POINTS_RATE: number = 100;


export interface DrawSampler {

  getSample(time: number): number;
  setDrawDuration(nbPoints: number, duration: number): void;

}


export class DrawSamplerSimple implements DrawSampler {

  constructor(public sample = INITIAL_POINTS) { }

  getSample(time: number): number {
    return this.sample;
  }

  setDrawDuration(nbPoints: number, duration: number): void {
  }

}


export class DrawSamplerDynamic implements DrawSampler {

  drawSpeed: number;

  getSample(time: number): number {
    return this.drawSpeed ? REFRESH_DELAY * this.drawSpeed : INITIAL_POINTS;
  }

  setDrawDuration(nbPoints: number, duration: number): void {
    this.drawSpeed = nbPoints / duration;
  }

}


export class DrawSamplerStatic implements DrawSampler {

  lastRefresh: number;

  constructor(public pointsRate = DEFAULT_POINTS_RATE) { }

  getSample(time: number): number {
    let start = this.lastRefresh;
    this.lastRefresh = time;
    return start ? this.pointsRate * (time - start) : 0;
  }

  setDrawDuration(nbPoints: number, duration: number): void { }

}


export class DrawSamplerOpti implements DrawSampler {

  lastRefresh: number;
  sample = INITIAL_POINTS;

  getSample(time: number): number {
    let start = this.lastRefresh;
    this.lastRefresh = time;
    if (start) {
      let coef = REFRESH_DELAY / (time - start);
      this.sample = Math.ceil(coef * this.sample)
    }
    return Math.max(INITIAL_POINTS, this.sample);
  }

  setDrawDuration(nbPoints: number, duration: number): void { }

}
