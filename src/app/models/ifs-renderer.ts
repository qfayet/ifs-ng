import { Point, Color, Ifs } from './ifs-model';

const INITIAL_SAMPLE = 1000;

const COLOR_SCALE = true;


export class IfsIterator {

  point = new Point();

  constructor(
    public ifs: Ifs,
    public color = new Color(),
    initialSample = INITIAL_SAMPLE,
  ) {
    for (let i = 0; i < initialSample; i++) {
      this.iterate();
    }
  }

  iterate() {
    let res = this.ifs.iterate(this.point, this.color, COLOR_SCALE);
    this.point = res.point;
    this.color = res.color;
  }

}


export interface IfsRenderer {

  drawIteration(ctx: CanvasRenderingContext2D): void;
  drawFinish(ctx: CanvasRenderingContext2D): void;

}


export class IfsRendererSimple implements IfsRenderer {

  iterator: IfsIterator;

  constructor(
    public ifs: Ifs,
    public width: number,
    public height: number,
  ) {
    this.iterator = new IfsIterator(ifs);
  }

  drawIteration(ctx: CanvasRenderingContext2D): void {
    this.iterator.iterate();
    let viewPoint = this.ifs.toView(this.iterator.point, this.width, this.height);
    ctx.fillStyle = this.iterator.color.format();
    ctx.fillRect(Math.ceil(viewPoint.x), Math.ceil(viewPoint.y), 1, 1);
  }

  drawFinish(ctx: CanvasRenderingContext2D): void { }

}


export class IfsPixel {

  pointCount = 0;
  colorSum = new Color();

  addPoint(color: Color): number {
    this.colorSum = this.colorSum.add(color);
    this.pointCount += 1;
    return this.pointCount;
  }

}


export class IfsImage {

  pixels: IfsPixel[][];
  pointCountMax = 0;

  constructor(
    public ifs: Ifs,
    public width: number,
    public height: number,
  ) {
    this.pixels = new Array<IfsPixel[]>(width);
    for (let i = 0; i < width; i++) {
      this.pixels[i] = new Array<IfsPixel>(height);
      for (let j = 0; j < height; j++) {
        this.pixels[i][j] = new IfsPixel();
      }
    }
  }

  getViewPoint(point: Point): Point {
    let viewPoint = this.ifs.toView(point, this.width, this.height);
    if (viewPoint.x < 0 || viewPoint.x >= this.width || viewPoint.y < 0 || viewPoint.y >= this.height) {
      return null;
    }
    return new Point(Math.floor(viewPoint.x), Math.floor(viewPoint.y));
  }

  addPoint(i: number, j: number, color: Color): void {
    let pointCount = this.pixels[i][j].addPoint(color);
    this.pointCountMax = Math.max(this.pointCountMax, pointCount);
  }

  getPixelColor(pixel: IfsPixel): Color {
    let coef = pixel.pointCount / this.pointCountMax;
    coef = Math.min(coef * this.ifs.intensity, 1.0);
    coef = Math.pow(coef, this.ifs.contrast);
    let bg = this.ifs._background.scale(1 - coef);
    return bg.add(pixel.colorSum.scale(coef / pixel.pointCount));
  }

  drawPixel(i: number, j: number, ctx: CanvasRenderingContext2D): void {
    let color = this.getPixelColor(this.pixels[i][j]);
    ctx.fillStyle = color.format();
    ctx.fillRect(i, j, 1, 1);
  }

  draw(ctx: CanvasRenderingContext2D): void {
    ctx.fillStyle = this.ifs._background.format();
    ctx.fillRect(0, 0, this.width, this.height);
    for (let i = 0; i < this.width; i++) {
      for (let j = 0; j < this.height; j++) {
        if (this.pixels[i][j].pointCount > 0) {
          this.drawPixel(i, j, ctx);
        }
      }
    }
  }

}


export class IfsRendererScale implements IfsRenderer {

  iterator: IfsIterator;
  image: IfsImage;

  constructor(ifs: Ifs, width: number, height: number) {
    this.iterator = new IfsIterator(ifs);
    this.image = new IfsImage(ifs, width, height);
  }

  drawIteration(ctx: CanvasRenderingContext2D): void {
    this.iterator.iterate();
    let viewPoint = this.image.getViewPoint(this.iterator.point);
    if (viewPoint !== null) {
      this.image.addPoint(viewPoint.x, viewPoint.y, this.iterator.color);
      this.image.drawPixel(viewPoint.x, viewPoint.y, ctx);
    }
  }

  drawFinish(ctx: CanvasRenderingContext2D): void { }

}
