
function byteToHex(byte: number): string {
  let hex = byte.toString(16).toUpperCase();
  return hex.length == 1 ? "0" + hex : hex;
}

function byteToFloat(byte: number): number {
  return byte / 256;
}

function floatToByte(byte: number): number {
  return Math.round(byte * 256);
}

function rgbToHex(r: number, g: number, b: number) {
  return `#${byteToHex(r)}${byteToHex(g)}${byteToHex(b)}`
}

function hexToRgb(hex: string): {r: number, g: number, b: number} {
  hex = hex.toUpperCase().replace(/^#/, "");
  if (hex.length === 3) {
    hex = hex.replace(/./g, match => match + "0");
  }
  var result = /^([0-9A-F]{2})([0-9A-F]{2})([0-9A-F]{2})$/i.exec(hex);
  return {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  }
}

function dist(x: number, y: number) {
  return Math.sqrt(x * x + y * y);
}


export interface Serializable {

  parse(data: any): void;
  format(): any;

}


export class SerializableImpl implements Serializable {

  decode(): void { }

  encode(): void { }

  parse(data: any): void {
    Object.assign(this, data);
    this.decode();
  }

  format(): any {
    let data = {};
    this.encode();
    Object.keys(this)
      .filter(key => !key.startsWith("_"))
      .forEach(key => data[key] = this[key]);
    return data
  }

}


export class Point {

  constructor(
    public x = 0,
    public y = 0,
  ) { }

}


export class Bounds extends SerializableImpl {

  static parse(data: any): Bounds {
    let obj = new Bounds();
    obj.parse(data);
    return obj;
  }

  constructor(
    public xMin = 0,
    public xMax = 0,
    public yMin = 0,
    public yMax = 0,
  ) {
    super();
  }

  getWidth(): number {
    return this.xMax - this.xMin;
  }

  getHeight(): number {
    return this.yMax - this.yMin;
  }

  toView(pnt: Point, width: number, height: number): Point {
    let coefX = (pnt.x - this.xMin) / this.getWidth();
    let coefY = (pnt.y - this.yMin) / this.getHeight();
    return new Point(coefX * width, coefY * height);
  }

}


export class Color implements Serializable {

  static parse(data: any): Color {
    let obj = new Color();
    obj.parse(data);
    return obj;
  }

  constructor(
    public r = 0,
    public g = 0,
    public b = 0,
  ) { }

  parse(data: any): void {
    let rgb = hexToRgb(data);
    this.r = byteToFloat(rgb.r);
    this.g = byteToFloat(rgb.g);
    this.b = byteToFloat(rgb.b);
  }

  format(): any {
    return rgbToHex(
      floatToByte(this.r),
      floatToByte(this.g),
      floatToByte(this.b),
    );
  }

  scale(coef: number): Color {
    return new Color(
      this.r * coef,
      this.g * coef,
      this.b * coef,
    );
  }

  add(color: Color): Color {
    return new Color(
      this.r + color.r,
      this.g + color.g,
      this.b + color.b,
    );
  }

}


export class IfsFunction extends SerializableImpl {

  static parse(data: any): IfsFunction {
    let obj = new IfsFunction();
    obj.parse(data);
    return obj;
  }

  coefs = [[0, 0, 0], [0, 0, 0]];
  proba = 1;
  _color = new Color();
  color: string;
  _colorScale: number;

  constructor() {
    super();
    this.updateColorScale();
  }

  updateColorScale(): void {
    this._colorScale = this.computeColorScale();
  }

  computeColorScale(): number {
    let scale1 = dist(this.coefs[0][0], this.coefs[1][0]);
    let scale2 = dist(this.coefs[0][1], this.coefs[1][1]);
    return Math.min((scale1 + scale2) / 2, 1);
  }

  decode() {
    this._color = Color.parse(this.color);
    this.updateColorScale();
  }

  encode() {
    this.color = this._color.format();
  }

  transformPoint(point: Point): Point {
    let x = this.coefs[0][0] * point.x + this.coefs[0][1] * point.y + this.coefs[0][2];
    let y = this.coefs[1][0] * point.x + this.coefs[1][1] * point.y + this.coefs[1][2];
    return new Point(x, y);
  }

  transformColor(color: Color): Color {
    let c1 = this._color.scale(this._colorScale);
    let c2 = color.scale(1 - this._colorScale);
    return c1.add(c2);
  }

}


export class Ifs extends SerializableImpl {

  static parse(data: any): Ifs {
    let obj = new Ifs();
    obj.parse(data);
    return obj;
  }

  name: string;
  _functions: IfsFunction[] = [];
  functions: any[];
  _bounds = new Bounds();
  bounds: any;
  _background = new Color();
  background: string;
  fill_frame = false;
  intensity = 1;
  contrast = 1;
  _probaSum: number;

  constructor() {
    super();
    this.updateProbaSum();
  }

  updateProbaSum(): void {
    this._probaSum = this._functions.reduce((sum, current) => sum + current.proba, 0);
  }

  decode() {
    this._functions = this.functions.map(elt => IfsFunction.parse(elt));
    this._bounds = Bounds.parse(this.bounds);
    this._background = Color.parse(this.background);
    this.updateProbaSum();
  }

  encode() {
    this.functions = this._functions.map(fct => fct.format());
    this.bounds = this._bounds.format();
    this.background = this._background.format();
  }

  randomFunction(): IfsFunction {
    let random = Math.random() * this._probaSum;
    let probaCount = 0;
    return this._functions.find((current) => {
      probaCount += current.proba;
      return random < probaCount;
    });
  }

  iterate(point: Point, color: Color, colorScale = false): {point: Point, color: Color} {
    let fct = this.randomFunction()
    return {
      point: fct.transformPoint(point),
      color: colorScale ? fct.transformColor(color) : fct._color,
    }
  }

  toView(pnt: Point, width: number, height: number) {
    return this._bounds.toView(pnt, width, height);
  }

}
